let projectFolder = 'dist';
let sourceFolder = 'app';

let path = {
  build: {
    html: projectFolder + '/',
    css: projectFolder + '/css/',
    js: projectFolder + '/js/',
    images: projectFolder + '/images/',
    fonts: projectFolder + '/fonts/',
  },
  src: {
    html: sourceFolder + '/*.html',
    css: sourceFolder + '/scss/**/*.scss',
    js: sourceFolder + '/js/*.js',
    images: sourceFolder + '/images/**/*.(jpg, png,svg,gif,ico,webp)',
    fonts: sourceFolder + '/fonts/*.ttf',
  },
  watch: {
    html: sourceFolder + '/**/*.html',
    css: sourceFolder + '/scss/**/*.scss',
    js: sourceFolder + '/js/**/*.js',
    images: sourceFolder + '/images/**/*.(jpg, png,svg,gif,ico,webp)',
  },
  clean: './' + projectFolder + '/',
};

const { src, dest, series, parallel } = require('gulp');
const gulp = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const browsersync = require('browser-sync').create();
const delFiles = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify-es').default;

function browserSyncServe(cb) {
  // initializes browsersync server
  browsersync.init({
    server: {
      baseDir: './' + projectFolder + '/',
    },
    notify: {
      styles: {
        top: 'auto',
        bottom: '0',
      },
    },
  });
  cb();
}

function images() {
  return src('app/images/**/*')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
        }),
      ])
    )
    .pipe(dest('dist/images'));
}

function html() {
  return src(path.src.html)
    .pipe(dest(path.build.html))
    .pipe(browsersync.stream());
}

function styles() {
  return src(path.src.css)
    .pipe(scss({ outputStyle: 'compressed' }))
    .pipe(concat('style.min.css'))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ['last 10 versions'],
        grid: true,
      })
    )
    .pipe(dest(path.build.css))
    .pipe(browsersync.stream());
}

function scripts() {
  return src(path.src.js)
    .pipe(concat('script.js'))
    .pipe(dest(path.build.js))
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream());
}

function watchFiles() {
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.css], styles);
  gulp.watch([path.watch.js], scripts);
}

function clean() {
  return delFiles(path.clean);
}

const build = series(clean, images, parallel(styles, html, scripts));
const watch = parallel(build, watchFiles, browserSyncServe);

exports.images = images;
exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.build = build;
exports.watch = watch;
exports.default = watch;

document.querySelector('.burger').addEventListener('click', (e) => {
  document.querySelector('.menu__list').classList.toggle('active');
  e.target.classList.toggle('fa-bars');
  e.target.classList.toggle('fa-times');
});
